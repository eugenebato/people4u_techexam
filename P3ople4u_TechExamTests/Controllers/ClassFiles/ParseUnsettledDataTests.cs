﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using P3ople4u_TechExam.Controllers.ClassFiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using P3ople4u_TechExam.Controllers;
using P3ople4u_TechExam.Models;

namespace P3ople4u_TechExam.Controllers.ClassFiles.Tests
{
    [TestClass()]
    public class ParseUnsettledDataTests
    {
        [TestMethod()]
        public void ParseUnsettledDataTest()
        {
            List<UnusualRateModel> resultList = new List<UnusualRateModel>();

            resultList = ParseUnsettledData.ParseData();

            Assert.IsTrue(resultList.Count > 0);
        }

        [TestMethod()]
        public void ParseSettledDataTest()
        {
            List<UnusualRateModel> resultList = ParseSettledData.ParseData();

            Assert.IsTrue(resultList.Count == 0);
        }
    }
}