﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace P3ople4u_TechExam.Models
{
    public class UnusualRateModel
    {
        public int Customer { get; set; }
        public int Event { get; set; }
        public int Participant { get; set; }
        public int Stake { get; set; }
        public int Win { get; set; }
        public string Remarks { get; set; }
    }
}