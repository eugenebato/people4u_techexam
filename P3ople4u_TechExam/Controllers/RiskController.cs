﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using P3ople4u_TechExam.Models;
using P3ople4u_TechExam.Controllers.ClassFiles;

namespace P3ople4u_TechExam.Controllers
{
    public class RiskController : Controller
    {
        //
        // GET: /Risk/
        public ActionResult Index()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Settled", Value = "Settled" });
            items.Add(new SelectListItem { Text = "Unsettled", Value = "Unsettled" });
            ViewData["Options"] = items;

            return View();
        }

        [HttpPost]
        public ActionResult CheckRates(FormCollection form)
        {
            List<UnusualRateModel> resultList = new List<UnusualRateModel>();

            string dataType = form["Options"].ToString();

            if (dataType == "Settled")
            {
                resultList = ParseSettledData.ParseData();
                ViewBag.Title = "Settled Bet Unusual Data";
            }
            else
            {
                resultList = ParseUnsettledData.ParseData();
                ViewBag.Title = "Unsettled Bet Unusual Data";
            }

            return View(resultList);
        }
	}
}