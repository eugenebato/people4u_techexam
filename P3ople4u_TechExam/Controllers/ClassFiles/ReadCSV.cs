﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using P3ople4u_TechExam.Models;

namespace P3ople4u_TechExam.Controllers.ClassFiles
{
    public class ReadCSV
    {
        public static List<UnusualRateModel> Read(string fileName)
        {
            List<UnusualRateModel> returnList = new List<UnusualRateModel>();

            string directory = AppDomain.CurrentDomain.BaseDirectory;

            directory = (directory.Substring(directory.Length-1, 1) != @"\") ? directory + @"\" : directory;

            var contents = File.ReadAllText(directory + fileName + ".csv").Split('\n');

            foreach(string content in contents)
            {
                string[] arLine = content.Split(',');

                UnusualRateModel temp = new UnusualRateModel();
                temp.Remarks = "";

                for(int i=0; i<=arLine.Length-1; i++)
                {
                    switch(i)
                    {
                        case 0:
                            temp.Customer = Convert.ToInt32(arLine[i]);
                            break;
                        case 1:
                            temp.Event = Convert.ToInt32(arLine[i]);
                            break;
                        case 2:
                            temp.Participant = Convert.ToInt32(arLine[i]);
                            break;
                        case 3:
                            temp.Stake = Convert.ToInt32(arLine[i]);
                            break;
                        case 4:
                            temp.Win = Convert.ToInt32(arLine[i]);
                            break;
                    }
                }

                returnList.Add(temp);
            }

            return returnList;
        }
    }
}