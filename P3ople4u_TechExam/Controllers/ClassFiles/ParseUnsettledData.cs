﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using P3ople4u_TechExam.Models;

namespace P3ople4u_TechExam.Controllers.ClassFiles
{
    public class ParseUnsettledData
    {
        public static List<UnusualRateModel> ParseData()
        {
            List<UnusualRateModel> csvData = ReadCSV.Read("Unsettled");

            var returnAverageList = (from data in csvData
                              group data by data.Customer into g
                              select new { Customer = g.Key, Average = (g.Sum(x=>x.Stake) / g.Count()) }).ToList();

            var result2 = (from data in csvData
                           where data.Stake >= ((from tempData in returnAverageList
                                                where tempData.Customer == data.Customer
                                                select tempData.Average).First() * 30)
                           select data).ToList();

            var result1 = (from data in csvData
                           where data.Stake >= ((from tempData in returnAverageList
                                                where tempData.Customer == data.Customer
                                                select tempData.Average).First() * 10)
                           select data).ToList();

            var result = (from data in csvData
                          where data.Win >= 1000
                          select data).ToList();

            List<UnusualRateModel> returnData = new List<UnusualRateModel>();

            foreach(var resultTemp in result2)
            {
                UnusualRateModel tempModel = new UnusualRateModel();

                tempModel.Customer = resultTemp.Customer;
                tempModel.Event = resultTemp.Event;
                tempModel.Participant = resultTemp.Participant;
                tempModel.Stake = resultTemp.Stake;
                tempModel.Win = resultTemp.Win;
                tempModel.Remarks = "Highly Unusual";

                returnData.Add(tempModel);
            }

            foreach (var resultTemp in result1)
            {
                UnusualRateModel tempModel = new UnusualRateModel();

                tempModel.Customer = resultTemp.Customer;
                tempModel.Event = resultTemp.Event;
                tempModel.Participant = resultTemp.Participant;
                tempModel.Stake = resultTemp.Stake;
                tempModel.Win = resultTemp.Win;
                tempModel.Remarks = "Unusual";

                returnData.Add(tempModel);
            }

            foreach (var resultTemp in result)
            {
                UnusualRateModel tempModel = new UnusualRateModel();

                tempModel.Customer = resultTemp.Customer;
                tempModel.Event = resultTemp.Event;
                tempModel.Participant = resultTemp.Participant;
                tempModel.Stake = resultTemp.Stake;
                tempModel.Win = resultTemp.Win;
                tempModel.Remarks = "Win is more than 1000";

                returnData.Add(tempModel);
            }

            return returnData;
        }
    }
}