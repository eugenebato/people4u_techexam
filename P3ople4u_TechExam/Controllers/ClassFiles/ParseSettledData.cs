﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using P3ople4u_TechExam.Models;

namespace P3ople4u_TechExam.Controllers.ClassFiles
{
    public class ParseSettledData
    {
        public static List<UnusualRateModel> ParseData()
        {
            List<UnusualRateModel> csvData = ReadCSV.Read("Settled");

            List<UnusualRateModel> returnData = csvData.Where(x => (x.Win > 0) && ((x.Stake / x.Win) > 0.60)).ToList();

            return returnData;
        }
    }
}